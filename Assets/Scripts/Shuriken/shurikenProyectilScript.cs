﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class shurikenProyectilScript : MonoBehaviour
{

    public float fuerzaX = 1000f;
    public float fuerzaY = 0; //los ninjas lanzan en línea recta, por lo que Y tiende a 0
    
    public Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _rigidbody2D.velocity = transform.right * fuerzaX;
    }

    private void FixedUpdate() //en físicas es mejor fixed update
    {
        
    }


    private void OnTriggerEnter2D(Collider2D impact)
    {
        if (impact.gameObject.tag == "enemy")
        {
            if (SceneManager.GetActiveScene().name == "Level3Scene")
            {
                impact.gameObject.GetComponent<CombateBoss>().vidas--;
                
            }
            else
            {
                impact.gameObject.GetComponent<EnemyCombat>().vidas--;
                
            }
            
        }

        Debug.Log(impact); //para saber contra que ha chocado antes de destruirse
        Destroy(gameObject);    

    }
}
