﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shurikenParaRecogerScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "protagonista")
        {
            Destroy(gameObject);
            Debug.Log("has recogido un shuriken! Hyaaaa!!!");
            GameManager.instance.municion++;
        }
    }
}