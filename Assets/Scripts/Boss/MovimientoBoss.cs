﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoBoss : MonoBehaviour
{
    public bool start = false;

    private int speed = 50;

    public GameObject protagonista;

    private Vector3 oldPosition, newPosition;
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        oldPosition = transform.position;
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            _animator.SetTrigger("dejaIdle");
            Movimiento();
        }
    }

    private void Movimiento()
    {
        _animator.SetBool("moviendo", true);
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(protagonista.transform.position.x, transform.position.y),
            speed * Time.deltaTime);

        if (protagonista.transform.position.x < transform.position.x)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
        else
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }
}