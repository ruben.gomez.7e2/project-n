﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorDeAtaque : MonoBehaviour
{
    public bool ataca = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "protagonista")
        {
            ataca = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "protagonista")
        {
            ataca = false;
        }
    }
}