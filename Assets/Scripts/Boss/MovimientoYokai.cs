﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoYokai : MonoBehaviour
{
    private int speed = 30;

    public GameObject protagonista;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position,
            new Vector2(protagonista.transform.position.x, transform.position.y),
            speed * Time.deltaTime);

        if (protagonista.transform.position.x < transform.position.x)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
        else
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        }
    }

    
}