﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateBoss : MonoBehaviour
{
    private DetectorDeAtaque detector;

    private Animator _animator;
    private MovimientoBoss _movimientoBoss;
    private bool cooldown = false;
    public GameObject congratulations;

    public int vidas = 10;

    // Start is called before the first frame update
    void Start()
    {
        detector = GetComponentInChildren<DetectorDeAtaque>();
        _animator = GetComponent<Animator>();
        _movimientoBoss = GetComponent<MovimientoBoss>();
    }

    // Update is called once per frame
    void Update()
    {
        if (detector.ataca)
        {
            ataca();
        }

        if (vidas <= 5)
        {
            _animator.SetTrigger("ulti");
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            _animator.SetTrigger("ulti");
        }

        if (vidas <= 0)
        {
            _movimientoBoss.start = false;
            StartCoroutine(FinDelJuego());
        }
    }

    private IEnumerator FinDelJuego()
    {
        
        _animator.SetTrigger("muerte");
        yield return new WaitForSeconds(6);
        congratulations.SetActive(true);
        Destroy(gameObject);
    }


    private void ataca()
    {
        _movimientoBoss.start = false;
        if (!cooldown)
        {
            _animator.SetTrigger("ataque");
        }

        _animator.SetBool("moviendo", false);
        StartCoroutine(finAtaque());
    }

    private IEnumerator finAtaque()
    {
        cooldown = true;
        yield return new WaitForSeconds(1f);
        _movimientoBoss.start = true;
        yield return new WaitForSeconds(1f);
        cooldown = false;
    }

 
}