﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ultimate : MonoBehaviour
{
    public GameObject yokai;
    private Animator _animator;
    private bool ultiHecha = false;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponentInParent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<CombateBoss>().vidas == 5 && !ultiHecha)
        {
            GetComponentInParent<MovimientoBoss>().start = false;
            Ultimate();
            ultiHecha = true;
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            GetComponentInParent<MovimientoBoss>().start = false;
            Ultimate();
        }
    }

    private void Ultimate()
    {
        StartCoroutine(invocacion());
    }

    private IEnumerator invocacion()
    {
        yield return new WaitForSeconds(4f);
        for (int i = 0; i < 5; i++)
        {
            Instantiate(yokai, transform.position, transform.rotation);
            yield return new WaitForSeconds(1f);
        }
        _animator.SetTrigger("ultiAcabada");
        yield return new WaitForSeconds(1.5f);
        GetComponentInParent<MovimientoBoss>().start = true;
        
    }
}