﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    private GameObject protagonista;

    private Rigidbody2D pjRB;

    private Camera _camera;
    // Start is called before the first frame update

    void Start()
    {
        protagonista = GameObject.Find("Protagonista");
        pjRB = protagonista.GetComponent<Rigidbody2D>();
        _camera = gameObject.GetComponent<Camera>();
        Color color = new Color(228,39,247);
        if (SceneManager.GetActiveScene().name == "Level2Scene")
        {
            _camera.backgroundColor = color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var transformPosition = transform.position;
        transformPosition.x = protagonista.transform.position.x;
        if (protagonista.transform.position.y >= 50 && pjRB.velocity.y > 1)
        {
            transformPosition = Vector3.Lerp(transformPosition, protagonista.transform.position,
                5f / Vector3.Distance(transformPosition, protagonista.transform.position));
        }
        else
        {
            transformPosition = Vector3.Lerp(transformPosition, protagonista.transform.position,
                40f / Vector3.Distance(transformPosition, protagonista.transform.position));
        }

        this.transform.position = transformPosition;
        
    }

    private void FixedUpdate()
    {
        var transformPosition = transform.position;
        transformPosition.z = -1000;
        transform.position = transformPosition;
    }
}