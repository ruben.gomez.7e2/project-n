﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoProtagonista : MonoBehaviour
{
    public Vector3 _fallCheckpoint;
    // Start is called before the first frame update
    void Start()
    {
        _fallCheckpoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Colision detected. Tag? " + other.gameObject.tag);
        if (other.gameObject.tag == "fallCheckpoint")
        {
            Debug.Log("Checkpoint!");
            _fallCheckpoint = other.transform.position;
        }

        if (other.gameObject.tag == "death")
        {
            Debug.Log("Death!");
            GameManager.instance.vidas--;
            if (GameManager.instance.vidas > 0)
            {
                transform.position = _fallCheckpoint;
            }
        }
    }
}