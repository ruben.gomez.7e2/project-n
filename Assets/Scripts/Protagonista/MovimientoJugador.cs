using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngineInternal;

public class MovimientoJugador : MonoBehaviour
{
    public float velocidad = 0f;
    private Vector3 _displacement;
    private Rigidbody2D _rigidbody;
    public float rcDistance = 33f;
    public LayerMask layerCollisionGround;
    public LayerMask layerCollisionBambu;
    public int saltosDados = 1, cantidadSaltos = 2;
    public int potenciaSalto = 100;
    private SpriteRenderer sprite;
    private Animator _animator;

    public bool agachada = false;

    public bool oculta = false;
    public bool puedoOcultar = false;
    public bool isDead = false;

    private bool haSaltado = false;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Salto();
        Debug.DrawLine(_rigidbody.position, new Vector3(_rigidbody.position.x, _rigidbody.position.y - rcDistance, 0),
            Color.red);
        TienesTripleSalto();
        PersonajeAgachada();
        Ocultacion();
        AnimacionBambu();
        IsDead();
        if (isDead)
        {
            StartCoroutine(CorotinaMuerte());
        }
    }

    private void FixedUpdate()
    {
        MovimientoHorizontal();
    }

    private void Ocultacion()
    {
        if (agachada && puedoOcultar)
        {
            oculta = true;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            agachada = false;
        }

        if (agachada == false)
        {
            oculta = false;
        }

        if (agachada == false)
        {
            oculta = false;
        }

        if (oculta)
        {
            sprite.sortingOrder = -10;
            gameObject.tag = "oculta";
        }

        else if (!oculta)
        {
            sprite.sortingOrder = 0;
            gameObject.tag = "protagonista";
        }
    }

    //Ocultacion
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ocultable")
        {
            puedoOcultar = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ocultable" && puedoOcultar)
        {
            //flag que NO tiene permiso para ocultarse
            puedoOcultar = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("aEscena2"))
        {
            SceneManager.LoadScene("Level2Scene");
        }

        if (other.tag.Equals("aEscena3"))
        {
            SceneManager.LoadScene("Level3Scene");
        }
    }

    private void PersonajeAgachada()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (agachada)
            {
                _animator.SetBool("agacharse", false);
                agachada = false;
            }
            else
            {
                _animator.SetBool("agacharse", true);
                agachada = true;
            }
        }
    }

    private void TienesTripleSalto()
    {
        if (GameManager.instance.tripleSaltoPowerUp == true)
        {
            cantidadSaltos = 3;
        }
    }

    private void Salto()
    {
        if (Input.GetKeyDown(KeyCode.Space) && (EstaEnElSuelo() || (cantidadSaltos > saltosDados)))
        {
            haSaltado = true;
            saltosDados++;
            _animator.SetTrigger("salta");
            _animator.SetBool("estaEnBambu", false);
            _rigidbody.AddForce(new Vector2(0, potenciaSalto + (-_rigidbody.velocity.y)), ForceMode2D.Impulse);
        }

        if (EstaEnElSuelo() || EstaEnBambu())
        {
            Debug.Log("Suelo");
            saltosDados = 1;
        }

        if (EstaEnElSuelo() && haSaltado && _rigidbody.velocity.y < 5)
        {
            _animator.SetTrigger("tocaSuelo");
            _animator.SetBool("estaEnBambu", false);
            haSaltado = false;
        }

        else if (EstaEnBambu() && haSaltado && _rigidbody.velocity.y < 5)
        {
            _animator.SetBool("estaEnBambu", true);
            haSaltado = false;
        }

        if (_rigidbody.velocity.y < -300)
        {
            var rigidbodyVelocity = _rigidbody.velocity;
            rigidbodyVelocity.y = -300;
            _rigidbody.velocity = rigidbodyVelocity;
        }


        if (_rigidbody.velocity.y < 5f && !EstaEnElSuelo())
        {
            _animator.SetBool("baja", true);
        }
    }

    private void MovimientoHorizontal()
    {
        if (!BlockMovement())
        {
            _displacement = new Vector3(Input.GetAxis("Horizontal"), 0);
            transform.position += (_displacement * (velocidad * Time.deltaTime));
            if (_displacement != new Vector3(0, 0, 0))
            {
                _animator.SetBool("movimiento", true);
            }
            else _animator.SetBool("movimiento", false);

            if (Input.GetKeyDown(KeyCode.A))
            {
                _animator.SetBool("izquierda", true);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                _animator.SetBool("izquierda", false);
            }
        }
    }

    private bool EstaEnElSuelo()
    {
        _animator.SetBool("baja", false);

        return Physics2D.Raycast(_rigidbody.position - new Vector2(11, 4), Vector2.down, rcDistance,
            layerCollisionGround);
        
        //
    }
    
    

    private void IsDead()
    {
        if (GameManager.instance.vidas == 0)
        {
            _animator.SetTrigger("isDead");
            isDead = true;
          
        }


    }

    IEnumerator CorotinaMuerte()
    {
        yield return new WaitForSeconds(4); //para que salte la animación de muerte
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        isDead = false;
        GameManager.instance.vidas = 5;
        GameManager.instance.municion = 5;
        yield break;
    }

    private bool EstaEnBambu()
    {
        return Physics2D.Raycast(_rigidbody.position, Vector2.down, rcDistance, layerCollisionBambu);
    }

    private void AnimacionBambu()
    {
        if (EstaEnBambu())
        {
            _animator.SetBool("estaEnBambu", true);
            
        }
        else _animator.SetBool("estaEnBambu", false);
    }


    private bool BlockMovement()
    {
        if (EstaEnBambu() || oculta)
        {
            Debug.Log("Bambu");
            return true;
        }

        else
        {
            return false;
        } 
    }
}