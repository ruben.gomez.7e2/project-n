﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombateProtagonista : MonoBehaviour
{
    private Animator _animator;

    public GameObject sword;
    private BoxCollider2D _boxCollider2D;
    public bool estaCombatiendo = false;
    public GameObject shuriken;
    public Transform firePointShuriken; //sin esta variable el shuriken se lanza desde una posición fija del juego siempre, sin lanzarlo desde el personaje
    private bool _disparando;
    private bool sigiloso;
    public bool atacandoSigiloso;
    private Rigidbody2D _rigidbody;
    public LayerMask layerCollision;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        sword.SetActive(false);
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<MovimientoJugador>().oculta && hayEnemigo())
        {
            ataqueSigiloso();
            atacandoSigiloso = true;
        }
        else
        {
            combate();
            atacandoSigiloso = false;
        }
    }

    private bool hayEnemigo()
    {
        return Physics2D.Raycast(_rigidbody.position, Vector2.right, 50, layerCollision);
    }

    private void ataqueSigiloso()
    {
        _animator.SetTrigger("ataqueTrigger");
    }

    private void combate()
    {
        if (Input.GetButtonDown("Fire1"))  //clic izquierdo
        {
            StartCoroutine(BloquearAnimacion());
            activarAnimacion();
            habilitarCollider();
            StartCoroutine(deshabilitarCollider());
            comprovarDaños();
            acabarAtaque();
        }
        
        if (Input.GetButtonDown("Fire2")&& GameManager.instance.municion>0 && !_disparando ) //clic derecho
        {

            LanzarShuriken();
            
        }
    }

    private void LanzarShuriken()
    {
        _disparando = true;
        Instantiate(shuriken, firePointShuriken.position, firePointShuriken.rotation);
        //shuriken.transform.position = new Vector3(gameObject.transform.position.x + 10f, gameObject.transform.position.y , 0); //el +10 es para que no aparezca justo encima de la protagonista
        GameManager.instance.municion--;
        _disparando = false;
        Debug.Log("Has lanzado un shuriken! Hya!");
    }

    private IEnumerator BloquearAnimacion()
    {
        estaCombatiendo = true;
        _animator.SetBool("animacionAtaqueAcabada", false);
        yield return new WaitForSeconds(0.79f);
        _animator.SetBool("animacionAtaqueAcabada", true);
        estaCombatiendo = false;
    }

    private IEnumerator deshabilitarCollider()
    {
        yield return new WaitForSeconds(0.75f);
        sword.SetActive(false);
    }

    private void acabarAtaque()
    {
    }

    private void comprovarDaños()
    {
    }

    private void habilitarCollider()
    {
        sword.SetActive(true);
    }

    private void activarAnimacion()
    {
        _animator.SetTrigger("ataqueTrigger");
    }

   
}