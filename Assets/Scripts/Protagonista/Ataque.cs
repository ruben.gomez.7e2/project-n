﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ataque : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "enemy")
        {
            if (GetComponentInParent<CombateProtagonista>().atacandoSigiloso)
            {
                Destroy(other.gameObject);
            }
            else
            {
                if (SceneManager.GetActiveScene().name == "Level3Scene")
                {
                    other.gameObject.GetComponent<CombateBoss>().vidas--;
                    Vector2 moveDirection = other.transform.position - transform.parent.position;
                    other.gameObject.GetComponent<Rigidbody2D>()
                        .AddForce(moveDirection.normalized * 100f, ForceMode2D.Impulse);
                }
                else
                {
                    other.gameObject.GetComponent<EnemyCombat>().vidas--;
                    Vector2 moveDirection = other.transform.position - transform.parent.position;
                    other.gameObject.GetComponent<Rigidbody2D>()
                        .AddForce(moveDirection.normalized * 100f, ForceMode2D.Impulse);
                }
            }
        }

        if (other.tag == "muerteFantasma")
        {
            Destroy(other.gameObject);
        }
    }
}