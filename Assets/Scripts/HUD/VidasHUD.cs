﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidasHUD : MonoBehaviour
{
    private SpriteRenderer numerosMetalicos;
    public Sprite[] numeros0A9;
    public int vidas;
    public bool unidades;
    public Text cantidadVidas;


    private void Start()
    {
        numerosMetalicos = GetComponent<SpriteRenderer>();
        cantidadVidas = GetComponent<Text>();
    }

    private void Update()
    {
        vidas = GameManager.instance.vidas;
        DisplayHp();
    }

    private void DisplayHp()
    {
        cantidadVidas.text = vidas.ToString();

    }


    /*
     
     
     //Al añadir el contador en formato sprite dentro del canvas ha dejado de funcionar, pero si no está en el canvas funciona
     //Al final el contador será en formato texto
     

     
    private void DisplayHp()
    {
        if (unidades)
        {
            int vidaRestante = vidas % 10;
            numerosMetalicos.sprite = numeros0A9[vidaRestante];
                
        }

        
        if (vidas == 0)
        {
            numerosMetalicos = null;
        }
        
    }
    
    
    */
}