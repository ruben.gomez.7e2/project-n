﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShurikensHUD : MonoBehaviour
{
    
    private SpriteRenderer numerosMetalicos;
    public Sprite[] numeros0A9;
    public int municionShurikens;
    public bool unidades;
    public Text cantidadShurikens;


    private void Start()
    {
        numerosMetalicos = GetComponent<SpriteRenderer>();
        cantidadShurikens = GetComponent<Text>();
    }

    private void Update()
    {
        municionShurikens = GameManager.instance.municion;
        DisplayAmmo();
    }

    private void DisplayAmmo()
    {
        cantidadShurikens.text = municionShurikens.ToString();
    }

    /*

    //Esto funciona si no está dentro del HUD, pero como está dando demasiados problemas arreglarlo la munición será en formato texto en lugar de Sprite

    private void DisplayAmmo()
    {
        if (unidades)
        {
            int municionRestante = municionShurikens % 10;
            numerosMetalicos.sprite = numeros0A9[municionRestante];
                
        }
        else
        {
            numerosMetalicos.sprite = numeros0A9[municionShurikens / 10];
        }
        
    }
    
    */
}