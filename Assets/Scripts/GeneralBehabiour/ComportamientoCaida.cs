﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoCaida : MonoBehaviour
{
    private GameObject protagonista;

    // Start is called before the first frame update
    void Start()
    {
        protagonista = GameObject.Find("Protagonista");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 transformPosition = transform.position;
        transformPosition.x = protagonista.transform.position.x;
        transform.position = transformPosition;
    }
}