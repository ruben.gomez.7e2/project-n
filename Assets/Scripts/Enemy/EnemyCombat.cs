﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : MonoBehaviour
{
    private bool combatiendo = false;

    private bool cooldown = false;

    public int vidas = 2;
    

    private Animator _animator;

    private Rigidbody2D _rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        combate();
        comprobarMuerte();
    }

    private void comprobarMuerte()
    {
        if (vidas <= 0)
        {
            _animator.SetTrigger("muerte");
            StartCoroutine(muerte());
        }
    }

    private IEnumerator muerte()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    private void combate()
    {
        if (combatiendo)
        {
            _animator.SetTrigger("ataque");
            StartCoroutine(ataqueCooldown());
        }
    }

    private IEnumerator ataqueCooldown()
    {
        combatiendo = false;
        cooldown = true;
        yield return new WaitForSeconds(1f);
        cooldown = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "protagonista" && !cooldown)
        {
            combatiendo = true;
        }
        
    }

    public bool GetCooldown()
    {
        return cooldown;
    }
}
