﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour
{

    public GameObject protagonista;
    private Rigidbody2D _rigidbody2DProtagonista;
    private EnemyCombat _enemyCombat;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2DProtagonista = protagonista.GetComponent<Rigidbody2D>();
        _enemyCombat = gameObject.GetComponentInParent<EnemyCombat>();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "protagonista" && !_enemyCombat.GetCooldown())
        {
            GameManager.instance.vidas--;
            Vector2 moveDirection = _rigidbody2DProtagonista.transform.position - transform.parent.position;
            _rigidbody2DProtagonista.AddForce( moveDirection.normalized * 100f, ForceMode2D.Impulse);
        }
    }
}
