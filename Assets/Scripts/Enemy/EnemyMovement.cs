using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class EnemyMovement : MonoBehaviour
{
    //Movement
    private Vector3 position;
    public float speed = 1f;
    public float weigth = 1f;
    public int direction = 1;

    private Rigidbody2D _rigidbody;
    public float rcDistanceX = 1f;
    public float rcDistanceXY = 1f;
    public LayerMask layerCollision;
    public bool perseguirEnemigo = false;
    public GameObject protagonista;
    private Animator _animator;
    private Vector3 _displacement;
    private bool estaAtacando = false;

    void Start()
    {
        _rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (!estaAtacando)
        {
             Movimiento();
             patrullaEnemigo();
             perseguirProtagonista();
        }
        //Movement
       
        Debug.DrawLine(_rigidbody.position, new Vector3(_rigidbody.position.x + 100, _rigidbody.position.y, 0),
            Color.red);
        Debug.DrawLine(_rigidbody.position, new Vector3(_rigidbody.position.x - 100, _rigidbody.position.y, 0),
            Color.blue);
        Animaciones();
    }

    private void Animaciones()
    {
        if (direction == -1)
        {
            _animator.SetBool("izquierda", true);
        }
        else _animator.SetBool("izquierda", false);

        if (_displacement != new Vector3(0, 0, 0))
        {
            _animator.SetBool("movimiento", true);
        }
        else _animator.SetBool("movimiento", false);
    }

    private void perseguirProtagonista()
    {
        if (perseguirEnemigo)
        {
            transform.position = Vector2.MoveTowards(transform.position, protagonista.transform.position,
                speed * Time.deltaTime);
        }
    }

    private void patrullaEnemigo()
    {
        if (direction == 1)
        {
            if (Physics2D.Raycast(_rigidbody.position, Vector2.right, 100, layerCollision) && !protagonista.GetComponent<MovimientoJugador>().oculta)
            {
                perseguirEnemigo = true;
            }
            else
            {
                perseguirEnemigo = false;
            }
        }

        if (direction == -1)
        {
            if (Physics2D.Raycast(_rigidbody.position, Vector2.left, 100, layerCollision) && !protagonista.GetComponent<MovimientoJugador>().oculta)
            {
                perseguirEnemigo = true;
            }
            else
            {
                perseguirEnemigo = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "patrol" && !perseguirEnemigo)
        {
            direction *= -1;
        }

        if (other.tag == "protagonista")
        {
            StartCoroutine(Atacando());
        }
    }

    private IEnumerator Atacando()
    {
        estaAtacando = true;
        yield return new WaitForSeconds(0.75f);
        estaAtacando = false;
    }

    private void Movimiento()
    {
        if (!perseguirEnemigo)
        {
   
            var movement = new Vector3(direction, 0);
            _displacement = movement * (speed / weigth) * Time.deltaTime;
            transform.position += movement * (speed / weigth) * Time.deltaTime;
        }
    }
}