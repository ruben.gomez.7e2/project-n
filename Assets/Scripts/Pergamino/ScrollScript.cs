﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrollScript : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.tag == "aEscena3" && other.tag == "protagonista")
        {
            StartCoroutine(escenaTres());
        }

        if (other.tag == "protagonista")
        {
            StartCoroutine(pasarEscena());
        }
        
    }

    private IEnumerator escenaTres()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Level3Scene");
    }

    private IEnumerator pasarEscena()
    {
        GameManager.instance.tripleSaltoPowerUp = true;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Level2Scene");
    }
}