﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeGemScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collisionGem)
    {
        Destroy(gameObject);
        Debug.Log("Notas como la gema te otorga vitalidad.");
        if (GameManager.instance.vidas < 5)
        {
            GameManager.instance.vidas++;
        }
        
    }
}
